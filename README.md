# PDI Bitrise Setup

This project is our costume step for bitrise that help us simplified send notification.

# Usage

Unfortunately, Bitrise Workflow Editor doesn't support editing private step when the script was written. we have to use bitrise.yml editor to add the step. The below is a snippet of adding the step into bitrise.yml

```yaml
workflows:
  test:
    steps:
      - git::https://bitbucket.org/powerdesigninc/pdi-bitrise-notification.git@main:
          title: Notification
          is_always_run: true
```

> `@main` can be replaced to a branch or tag. main is the production branch and use it, the workflow will always use latest version, but you can switch to old version if you want to.

# Inputs

| name                  | default value             |
| --------------------- | ------------------------- |
| notification_webhooks | CICD_Notification channel |

see [step.yml](./step.yml) for more detail.

# Test Locally

Bitrise runs the workflow in docker with its images like `bitriseio/docker-bitrise-base`. We can do the same thing to emulator its running environment.

```bash
docker run --rm -v $PWD:/bitrise/src bitriseio/docker-bitrise-base bitrise run test
```
