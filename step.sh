#!/usr/bin/env bash
set -e
#set -x

if [ -f "./package.json" ]; then
  VERSION=`node -p -e "require('./package.json').version"`-`git rev-list HEAD --count`;
else
  VERSION="$BITRISE_BUILD_NUMBER"
fi

if [ "$BITRISE_BUILD_STATUS" == "0" ]; then
  STATUS="Success"
  COLOR="#10c289"
else
  STATUS="Fail"
  COLOR="#ff2158"
fi

TITLE="$BITRISE_APP_TITLE: $STATUS"

MESSAGE="{
  \"@type\": \"MessageCard\",
  \"summary\": \"$TITLE\",
  \"themeColor\": \"$COLOR\",
  \"sections\": [{ 
    \"activityTitle\": \"$TITLE\", 
    \"facts\": [
      { \"name\": \"Build Number\", \"value\": \"$BITRISE_BUILD_NUMBER\" }, 
      { \"name\": \"Status\", \"value\": \"$STATUS\" }, 
      { \"name\": \"Workflow\", \"value\": \"$BITRISE_TRIGGERED_WORKFLOW_ID\" },
      { \"name\": \"Branch\", \"value\": \"$BITRISE_GIT_BRANCH\" }, 
      { \"name\": \"Commit\", \"value\": \"$BITRISE_GIT_MESSAGE\" }, 
      { \"name\": \"Version\", \"value\": \"$VERSION\" }
    ]
  }],
  \"potentialAction\": [
    {
      \"@type\": \"OpenUri\",
      \"name\": \"View\",
      \"targets\": [
        {
          \"os\": \"default\",
          \"uri\": \"$BITRISE_BUILD_URL\"
        }
      ]
    }
  ]
}"

IFS=';' read -ra URLS <<< "$notification_webhooks"
for URL in "${URLS[@]}" ; do
  URL=`echo $URL | xargs`
  if [ ! -z "$URL" ]; then
    curl -X POST $URL --header 'Content-Type: application/json' --data-raw "$MESSAGE" &>/dev/null
  fi
done

echo "Sent"